const fs = require("fs");
const builder = require("xmlbuilder");
const request = require("request");
const path = require("path");

let lang = ["cs", "en"];
// products

// XHR calls
const getAllData = async () => {
  const art = await getArtworks(lang[0], "art");
  const design = await getArtworks(lang[0], "design");
  createXml(art, "Umění");
  createXml(design, "Design");
};

getAllData();

function getArtworks(language, productType) {
  return new Promise(function(resolve, reject) {
    const productFilter = {
      productType: productType
    };
    const options = {
      url: `https://api.origoo.cz/product/all`,
      headers: {
        "Accept-Language": language
      },
      json: true,
      body: productFilter
    };
    request.post(options, function(error, response, body) {
      if (!error && response.statusCode === 200) {
        resolve(body.products);
      } else {
        reject(error);
      }
    });
  });
}
// TODO: neodpovidajici pocet umeleckych del v shoptetu a na webu
/*// Get content from file
const contents = fs.readFileSync("export.json");
// Define to JSON type .
const jsonContent = JSON.parse(contents); */
function createXml(data, requestType) {
  console.log("creating XML " + requestType);
  console.log(" objects: " + data.length);

  // change to objetcs writing
  let SHOPITEM = [];
  let all = data.length;

  for (let index = 0; index < all; index++) {
    let o = data[index];
    var item = {
      NAME: o.name,
      META_DESCRIPTION: o.metaDescription,
      DESCRIPTION: o.description,
      MANUFACTURER() {
        if (o.author.secondAuthor.lastName === null) {
          return o.author.firstName + " " + o.author.lastName;
        } else {
          let bothNames =
            o.author.firstName +
            " " +
            o.author.lastName +
            " & " +
            o.author.secondAuthor.firstName +
            " " +
            o.author.secondAuthor.lastName;
          return bothNames;
        }
      },
      ITEM_TYPE: "product",
      UNIT: "ks",
      CATEGORIES: {
        CATEGORY() {
          let cats = [];
          o.category.forEach(element => {
            cats.push(requestType + " > " + element.name);
          });
          if (o.category.length > 1) {
            return cats;
          } else {
            return requestType;
          }
        }
      },
      IMAGES: {
        IMAGE() {
          let images = [];
          o.pictures.forEach(element => {
           
              images.push(element.largeUrl);
          });
          return images;
        }
      },
      TEXT_PROPERTIES: {
        TEXT_PROPERTY() {
          let text_properties = [
            {
              NAME: "rok vyroby",
              VALUE: o.year
            },
            {
              NAME: "sirka • vyska",
              VALUE: o.dimensions.width + " * " + o.dimensions.height
            }
          ];
          // if price is on request
          if( o.priceOnRequest === true) {
            let additionalTag = [
              {
                NAME: "cena",
                VALUE: "na dotázání"
              }]
            text_properties.push(additionalTag)
          }
          return text_properties;
        }
      },
      // RELATED_PRODUCTS omitted
      // FLAGS Omitted
      CODE: "OR-" + o.id,
      PRICE(){
        if( o.priceOnRequest === true) {
        return o.price
        } else {
          return o.price
        }
      },
      // STANDARD PRICE omitted
      //PURCHASE_PRICE: o.price,
      // PRICE VAT omitted
      // WEIGHT:
      CURRENCY: "CZK",
      STOCK: {
        AMOUNT: 1,
        MINIMAL_AMOUNT: 1
      },
      AVAILABILITY: "Skladem"
    };

    SHOPITEM.push(item);
  }

  let obj = {
    SHOP: {
      SHOPITEM
    }
  };

  var xmlOutput = builder.create(obj).end({ pretty: true });
  let filename = "xml-result-" + requestType+".xml";
  fs.writeFile(filename, xmlOutput, function(err) {
    console.log('xml '+requestType+' written')
    if (err) return console.log(err);
  });
}
